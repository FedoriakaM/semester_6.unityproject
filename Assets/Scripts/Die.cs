﻿using UnityEngine;

public class Die : MonoBehaviour
{
    [SerializeField] Transform Respawn;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
            collision.transform.position = Respawn.position;

    }

}