﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Hero : MonoBehaviour
{
    private Rigidbody2D _rb;
    private Animator _animator;
    private bool _isGrounded;

    private const float StepSize = 0.025f;
    private const float TimeInterval = 0.0001f;

    // Use this for initialization
    void Start () {
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {

    }

    void OnCollisionEnter2D()
    {
        _isGrounded = true;
    }

    public void Jump(float height)
    {
        if (_isGrounded)
        {
            _rb.AddForce(new Vector2(0, height), ForceMode2D.Impulse);
            _isGrounded = false;
        }
    }

    public void Move(float distance)
    {
        if (distance < 0)
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        if (distance > 0)
            transform.localRotation = Quaternion.Euler(0, 0, 0);

        _animator.SetInteger("Anim", 2);
        StartCoroutine(MoveHelper(distance));
    }

    private IEnumerator MoveHelper(float distance)
    {
        if (distance > 0)
        {
            while (distance > 0)
            {
                yield return new WaitForSeconds(TimeInterval);
                distance -= StepSize;
                _rb.transform.position = new Vector2(_rb.position.x + StepSize, _rb.position.y);
            }
        }
        else
        {
            while (distance < 0)
            {
                yield return new WaitForSeconds(TimeInterval);
                distance += StepSize;
                _rb.transform.position = new Vector2(_rb.position.x - StepSize, _rb.position.y);
            }
        }
        _animator.SetInteger("Anim", 1);
    }
}
