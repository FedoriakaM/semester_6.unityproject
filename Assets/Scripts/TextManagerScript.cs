﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TextManagerScript : MonoBehaviour
{
    public InputField InputField;
    public Hero Hero;
    string[] _commands;

    private const float MoveSizeDuringJump = 2f;
    private const float MoveSize = 1f;
    private const float JumpHeight = 7;

    public void ExecuteCommand()
    {
        //StartCoroutine(Test());

        Debug.Log("Execute");
        _commands = InputField.text.Split('\n');
        Debug.Log(_commands.Length);
        StartCoroutine(ExecuteCode());
    }

    private IEnumerator ExecuteCode()
    {
        foreach (var command in _commands)
        {
            Debug.Log(command);
            switch (command)
            {
                case "jump":
                {
                    Hero.Jump(JumpHeight);
                    yield return new WaitForSeconds(3);
                }
                    break;
                case "jump left":
                {
                    Hero.Jump(JumpHeight);
                    Hero.Move(-MoveSizeDuringJump);
                    yield return new WaitForSeconds(3);
                }
                    break;
                case "jump right":
                {
                    Hero.Jump(JumpHeight);
                    Hero.Move(MoveSizeDuringJump);
                    yield return new WaitForSeconds(3);
                }
                    break;
                default:
                {
                    if (command.StartsWith("move left"))
                    {
                        if (command.Split(' ').Length == 3 && int.TryParse(command.Split(' ')[2], out var result))
                        {
                            Hero.Move(-result * MoveSize);
                            yield return new WaitForSeconds(3);
                        }
                    }
                    else if (command.StartsWith("move right"))
                    {
                        if (command.Split(' ').Length == 3 && int.TryParse(command.Split(' ')[2], out var result))
                        {
                            Hero.Move(result * MoveSize);
                            yield return new WaitForSeconds(3);
                        }
                    }
                }
                    break;
            }
        }
    }

    private IEnumerator Test()
    {
        //Hero.Move(10f);
        //yield return new WaitForSeconds(5);
        Hero.Jump(JumpHeight);
        Hero.Move(MoveSizeDuringJump);
        yield return new WaitForSeconds(5);
    }
}
